<?php

function theme_soundcloud_thumbnail($variables) {
  $output = '';

  $output .= '<span class="teaser teaser--playlist has-audio">';
  $output .= '<span class="teaser__title">' . check_plain($variables['title']) . '</span>';
  $output .= '</span>';

  return $output;
}