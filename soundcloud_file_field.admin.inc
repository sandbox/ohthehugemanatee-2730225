<?php
/**
 * @file
 * Admin form and related functions for soundcloud_file_field.
 */

function soundcloud_file_field_settings_form($form, &$form_state) {
  // Fieldset so it looks pretty.
  $form['soundcloud-file-field'] = array(
    '#type' => 'fieldset',
    '#title' => t('Soundcloud File Field settings'),
  );
  // Spoof audio URL field. But you probably set this in settings.php, didn't you?
  $form['soundcloud-file-field']['soundcloud_file_field_spoof_url'] = array(
    '#title' => t('Fake soundcloud permalink response'),
    '#description' => t('Enter a full Soundcloud track URL. For use in development environments where you can\'t access the Soundcloud API. This will be returned for every file uploaded with Soundcloud_file_field module.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('soundcloud_file_field_spoof_url', NULL),
  );

  // Our oauth2 connection options, from ctools.
  ctools_include('export');
  $exports = ctools_export_crud_load_all('oauth2_client_service_client', $reset = FALSE);
  $options = array();
  foreach ($exports as $delta => $export) {
    $options[$delta] = $export->name;
  }
  // A handy-dandy link to oauth admin interface.
  $oauth_link = l('OAuth2 Client Service module', 'admin/structure/oauth2-connections');

  // Select which oauth2 connection to use.
  $form['soundcloud-file-field']['soundcloud_file_field_oauth2_client'] = array(
    '#title' => t('OAuth2 Client'),
    '#description' => t('Select the oauth2 client configuration to use when connecting to Soundcloud. Configure your connections with the !oauthlink', array('!oauthlink' => $oauth_link)),
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => variable_get('soundcloud_file_field_oauth2_client', NULL),
  );

  // Authorize now, for the impatient.
  $form['soundcloud-file-field']['authorize'] = array(
    '#description' => t('Authorize with Soundcloud using the saved values.'),
    '#type' => 'submit',
    '#value' => t('Authorize'),
    '#name' => 'authorize-now',
  );

  // The submit handler that will do the authorization.
  $form['#submit'][] = 'soundcloud_file_field_authorize';

  // If we're not on HTTPS, don't let the user authorize... and make them feel bad about it.
  if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off') {
    //$form['soundcloud-file-field']['authorize']['#disabled'] = TRUE;

    $form['soundcloud-file-field']['authorize-message'] = array(
      '#markup' => t('You must access your site over HTTPS to authorize with soundcloud!'),
      '#prefix' => '<div id="#authorize-message">',
      '#suffix' => '</div>',
    );
  }

  return system_settings_form($form);
}

/**
 * Authorizes the site against soundcloud.
 */
function soundcloud_file_field_authorize($form, &$form_state) {
  if ($form_state['triggering_element']['#name'] == 'authorize-now') {
    // Authorize the site with Soundcloud, redirecting if necessary.
    $connection_id = $form_state['values']['soundcloud_file_field_oauth2_client'];
    $client = \Drupal::service('oauth2_client')
      ->getConnection($connection_id);
    if (!is_object($client)) {
      drupal_set_message('Could not authorize with Soundcloud, because your oauth connection is not valid.', 'warning');
    }
    else {
      if ($client->getAccessToken()) {
        drupal_set_message('Successfully authorized with Soundcloud');
      }
      else {
        drupal_set_message('Could not authorize with Soundcloud. Check your connection settings.');
      }
    }

  }
}