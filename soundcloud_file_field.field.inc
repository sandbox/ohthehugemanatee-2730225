<?php
/**
 * Field API stuff for soundcloud_file_field.
 */

define('SOUNDCLOUD_FILE_FIELD_DEFAULT_WIDTH', 100);
define('SOUNDCLOUD_FILE_FIELD_DEFAULT_HEIGHT', 81);
define('SOUNDCLOUD_FILE_FIELD_DEFAULT_HEIGHT_SETS', 305);
define('SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT', 166);
define('SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT_SETS', 450);
// Usable sizes for visual player: 300, 450, 600.
define('SOUNDCLOUD_FILE_FIELD_DEFAULT_VISUAL_PLAYER_HEIGHT', 450);


/**
 * Implements hook_field_info().
 */
function soundcloud_file_field_field_info() {
  return array(
    'soundcloud_file_field' => array(
      'label' => t('SoundCloud file'),
      'description' => t('Upload a file to be converted into a SoundCloud track.'),
      'instance_settings' => array(
        'title' => '',
        'description' => '',
        'tags' => '',
        'width' => SOUNDCLOUD_FILE_FIELD_DEFAULT_WIDTH,
        'height' => SOUNDCLOUD_FILE_FIELD_DEFAULT_HEIGHT,
        'height_sets' => SOUNDCLOUD_FILE_FIELD_DEFAULT_HEIGHT_SETS,
        'autoplay' => FALSE,
        'showcomments' => FALSE,
        'showplaycount' => FALSE,
        'showartwork' => FALSE,
        'color' => 'ff7700',
        'html5_player_height' => SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT,
        'html5_player_height_sets' => SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT_SETS,
        // New visual player setting.
        'visual_player_height' => SOUNDCLOUD_FILE_FIELD_DEFAULT_VISUAL_PLAYER_HEIGHT,
        'uri_scheme' => 'public',
        'file_directory' => '',
        'file_resup' => FALSE,
      ),
      'default_widget' => 'soundcloud_upload',
      'default_formatter' => 'soundcloud_default',
      'property_type' => 'soundcloud_file_field',
      'property_callbacks' => array('soundcloud_file_field_property_info_callback'),
    ),
  );
}


/**
 * Property callback for this field type.
 * @see hook_field_info().
 */
function soundcloud_file_field_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
  $name = $field['field_name'];
  $property = &$info[$entity_type]['bundles'][$instance['bundle']]['properties'][$name];

  $property['type'] = ($field['cardinality'] != 1) ? 'list<soundcloud_file_field>' : 'soundcloud_file_field';
  $property['getter callback'] = 'entity_metadata_field_verbatim_get';
  $property['setter callback'] = 'entity_metadata_field_verbatim_set';
  $property['property info'] = soundcloud_file_field_field_data_property_info();

  unset($property['query callback']);
}

/**
 * Defines info for the properties of soundcloud_file_field data.
 */
function soundcloud_file_field_field_data_property_info($name = NULL) {
  return array(
    'url' => array(
      'label' => t('soundcloud URL'),
      'description' => t('The absolute URL for the soundcloud video.'),
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
    'fid' => array(
      'label' => t('Audio file'),
      'description' => t('The audio file'),
      'type' => 'file',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
    'queued' => array(
      'label' => t('Queued timestamp'),
      'description' => t('The timestamp from when the file was queued to be uploaded'),
      'type' => 'integer',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    ),
  );
}

/**
 * Implements hook_field_validate().
 *
 * Pseudo-hook.
 */
function soundcloud_file_field_field_validate($entity_type, $entity, $field, $instance, $langcode, &$items, &$errors) {
  if ($field['type'] == 'soundcloud_file_field') {
    foreach ($items as $delta => $item) {
      if (!empty($item['url']) && !preg_match('@^https?://soundcloud\.com/([^"\&]+)@i', $item['url'], $matches)) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'soundcloud_file_field_invalid_url',
          'message' => t('%url is not a valid SoundCloud URL.', array('%url' => $item['url'])),
        );
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 *
 * Pseudo-hook.
 */
function soundcloud_file_field_field_is_empty($item, $field) {
  if (empty($item['url']) && empty($item['fid'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_settings_form().
 *
 * Pseudo-hook.
 */
function soundcloud_file_field_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  if ($field['type'] == 'soundcloud_file_field') {

    // Build a list of token types that make sense for this field instance.
    $token_types = array('global');
    // Token type names != entity type names, so there's a mapping function.
    $token_type_for_this_entity_type = token_get_entity_mapping('entity', $instance['entity_type']);
    if ($token_type_for_this_entity_type) {
      $token_types[] = $token_type_for_this_entity_type;
    }

    $form = array(
      '#element_validate' => array('soundcloud_file_field_instance_settings_form_validate'),
    );

    $scheme_options = array();
    foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
      $scheme_options[$scheme] = $stream_wrapper['name'];
    }
    $form['uri_scheme'] = array(
      '#type' => 'radios',
      '#title' => t('Upload destination'),
      '#options' => $scheme_options,
      '#default_value' => $settings['uri_scheme'],
      '#description' => t('Select where the final files should be stored. Private file storage has significantly more overhead than public files, but allows restricted access to files within this field.'),
    );

    $form['file_directory'] = array(
      '#type' => 'textfield',
      '#title' => t('File directory'),
      '#default_value' => $settings['file_directory'],
      '#description' => t('Optional subdirectory within the upload destination where files will be stored. Do not include preceding or trailing slashes.'),
      '#element_validate' => array('_file_generic_settings_file_directory_validate'),
    );

    $file_resup_link = l('File Resumable Upload', 'https://www.drupal.org/project/file_resup');
    $form['file_resup'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use File Resumable Uploads'),
      '#description' => t('Enable integration with !file_resup module.', array('!file_resup' => $file_resup_link)),
      '#default_value' => $settings['file_resup'],
      '#disabled' => !module_exists('file_resup'),
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Soundcloud title'),
      '#description' => t('The Title to be used on Soundcloud for uploaded tracks. This field supports tokens; see the list of available tokens below.'),
      '#element_validate' => array('token_element_validate'),
      '#default_value' => $settings['title'],
      '#token_types' => $token_types,
      '#required' => TRUE,
    );

    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Soundcloud description'),
      '#description' => t('The Description to be used on Soundcloud for uploaded tracks. This field supports tokens; see the list of available tokens below.'),
      '#default_value' => $settings['description'],
      '#token_types' => $token_types,
    );

    $form['tags'] = array(
      '#type' => 'textarea',
      '#title' => t('Soundcloud tags'),
      '#description' => t('A list of Tags to be used on Soundcloud for uploaded tracks. One tag per line. This field supports tokens; see the list of available tokens below.'),
      '#element_validate' => array('token_element_validate'),
      '#default_value' => $settings['tags'],
      '#token_types' => $token_types,
    );

    $form['tokens'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => $token_types,
    );

    $form['soundcloudplayer'] = array(
      '#type' => 'fieldset',
      '#title' => t('SoundCloud player settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['soundcloudplayer']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#size' => 4,
      '#default_value' => empty($settings['soundcloudplayer']['width']) ? SOUNDCLOUD_FILE_FIELD_DEFAULT_WIDTH : $settings['soundcloudplayer']['width'],
      '#description' => t('Player width in percent. Default is @width.', array('@width' => SOUNDCLOUD_FILE_FIELD_DEFAULT_WIDTH)),
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );

    // Flash player-only settings.
    $form['soundcloudplayer']['flash_player'] = array(
      '#type' => 'fieldset',
      '#title' => t('SoundCloud flash player-only settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['soundcloudplayer']['flash_player']['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height for tracks'),
      '#size' => 4,
      '#default_value' => empty($settings['soundcloudplayer']['flash_player']['height']) ? SOUNDCLOUD_FILE_FIELD_DEFAULT_HEIGHT : $settings['soundcloudplayer']['flash_player']['height'],
      '#description' => t('Player height for tracks. Default is @height.', array('@height' => SOUNDCLOUD_FILE_FIELD_DEFAULT_HEIGHT)),
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );
    $form['soundcloudplayer']['flash_player']['height_sets'] = array(
      '#type' => 'textfield',
      '#title' => t('Height for sets'),
      '#size' => 4,
      '#default_value' => empty($settings['soundcloudplayer']['flash_player']['height_sets']) ? SOUNDCLOUD_FILE_FIELD_DEFAULT_HEIGHT_SETS : $settings['soundcloudplayer']['flash_player']['height_sets'],
      '#description' => t('Player height for sets. Default is @height.', array('@height' => SOUNDCLOUD_FILE_FIELD_DEFAULT_HEIGHT_SETS)),
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );
    $form['soundcloudplayer']['flash_player']['showcomments'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show comments'),
      '#default_value' => empty($settings['soundcloudplayer']['flash_player']['showcomments']) ? FALSE : $settings['soundcloudplayer']['flash_player']['showcomments'],
      '#description' => t('Show comments by default in the player.'),
    );

    // HTML5 (classic) player-only settings.
    $form['soundcloudplayer']['html5_player'] = array(
      '#type' => 'fieldset',
      '#title' => t('SoundCloud HTML5 player-only settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['soundcloudplayer']['html5_player']['html5_player_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height for tracks'),
      '#size' => 4,
      '#default_value' => empty($settings['soundcloudplayer']['html5_player']['html5_player_height']) ? SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT : $settings['soundcloudplayer']['html5_player']['html5_player_height'],
      '#description' => t('Player height for tracks. Default is @height.', array('@height' => SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT)),
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );
    $form['soundcloudplayer']['html5_player']['html5_player_height_sets'] = array(
      '#type' => 'textfield',
      '#title' => t('Height for sets'),
      '#size' => 4,
      '#default_value' => empty($settings['soundcloudplayer']['html5_player']['html5_player_height_sets']) ? SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT_SETS : $settings['soundcloudplayer']['html5_player']['html5_player_height_sets'],
      '#description' => t('Player height for sets. Default is @height.', array('@height' => SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT_SETS)),
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );

    // Visual (new) player-only settings.
    $form['soundcloudplayer']['visual_player'] = array(
      '#type' => 'fieldset',
      '#title' => t('SoundCloud HTML5 visual player-only settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['soundcloudplayer']['visual_player']['visual_player_height'] = array(
      '#type' => 'select',
      '#title' => t('Size of the visual player (height):'),
      '#default_value' => empty($settings['soundcloudplayer']['visual_player']['visual_player_height']) ? SOUNDCLOUD_FILE_FIELD_DEFAULT_VISUAL_PLAYER_HEIGHT : $settings['soundcloudplayer']['visual_player']['visual_player_height'],
      '#options' => array(
        300 => '300px',
        450 => '450px',
        600 => '600px',
      ),
      '#description' => t('Height of the "new" HTML5 visual player.'),
    );

    // Common settings.
    $form['soundcloudplayer']['autoplay'] = array(
      '#type' => 'checkbox',
      '#title' => t('Autoplay'),
      '#default_value' => empty($settings['soundcloudplayer']['autoplay']) ? FALSE : $settings['soundcloudplayer']['autoplay'],
      '#description' => t('Autoplay track or set.'),
    );
    $form['soundcloudplayer']['showplaycount'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show play count'),
      '#default_value' => empty($settings['soundcloudplayer']['showplaycount']) ? FALSE : $settings['soundcloudplayer']['showplaycount'],
      '#description' => t('Show play count in player.'),
    );
    $form['soundcloudplayer']['showartwork'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show artwork'),
      '#default_value' => empty($settings['soundcloudplayer']['showartwork']) ? FALSE : $settings['soundcloudplayer']['showartwork'],
      '#description' => t('Show artwork in player. Has no effect when using the visual player.'),
    );
    $form['soundcloudplayer']['color'] = array(
      '#type' => module_exists('jquery_colorpicker') ? 'jquery_colorpicker' : 'textfield',
      '#title' => t('Player color'),
      '#default_value' => empty($settings['soundcloudplayer']['color']) ? 'ff7700' : $settings['soundcloudplayer']['color'],
      '#required' => TRUE,
      '#description' => t('Player color in hexadecimal format. Default is ff7700. Turn on the jQuery Colorpicker module if available.'),
    );
  }

  return $form;
}

/**
 * Validate the field settings form.
 */
function soundcloud_file_field_instance_settings_form_validate($element, &$form_state, $complete_form) {
  // Validate width.
  $width = $form_state['values']['instance']['settings']['soundcloudplayer']['width'];
  if (!empty($width) && ($width < 1 || $width > 100)) {
    form_set_error('instance][settings][soundcloudplayer][width', t('Player width must be a positive integer between 1-100'));
  }
}


/**
 * Implements hook_field_widget_info().
 * Add our Soundcloud Upload widget.
 */
function soundcloud_file_field_field_widget_info() {
  $info['soundcloud_upload'] = array(
    'label' => t('SoundCloud Upload'),
    'field types' => array('soundcloud_file_field'),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      'default value' => FIELD_BEHAVIOR_DEFAULT,
    ),
  );
  return $info;
}



/**
 * Implements hook_field_widget_form().
 * The actual field widget to be displayed in forms.
 */
function soundcloud_file_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  if ($instance['widget']['type'] == 'soundcloud_upload') {

    // Load the items for form rebuilds from the field state as they might not be
    // in $form_state['values'] because of validation limitations. Also, they are
    // only passed in as $items when editing existing entities.
    $field_state = field_form_get_state($element['#field_parents'], $field['field_name'], $langcode, $form_state);
    if (isset($field_state['items'])) {
      $items = $field_state['items'];
    }

    // Put it all into a fieldset so it looks purdy.
    $base = $element;
    $element += array(
      '#type' => 'fieldset',
      'collapsible' => FALSE,
      // Allows us to submit an array as a value.
      '#extended' => TRUE,
      '#element_validate' => array('_soundcloud_file_field_validate'),
      '#description' => t('If you provide a file and no URL, the file will be uploaded to Soundcloud automatically, and a URL will be generated for you.'),
    );

    // Essentially we use the managed_file type, extended with some enhancements.
    $element['file'] = array(
        '#title' => t('Audio file'),
        '#description' => t('Soundcloud accepts aiff, wav, flac, alac, ogg, mp2, mp3, aac, amr, and wma files, up to 5Gb.'),
        '#type' => 'managed_file',
        '#upload_location' => $instance['settings']['uri_scheme'] . '://' . $instance['settings']['file_directory'],
        '#upload_validators' => array(
          'file_validate_extensions' => array('aiff wav flac alac ogg mp2 mp3 aac amr wma'),
          'file_validate_size' => array(5*1024*1024*1024),
          ),
        '#progress_indicator' => 'bar',
        '#default_value' => isset($items[$delta]['fid']) ? $items[$delta]['fid'] : NULL,
        '#weight' => -1,
      ) + $base;

    // file_resup settings, if enabled and available.
    if (module_exists('file_resup') && user_access('upload via file_resup') && file_upload_max_size() >= file_resup_chunksize()
      && !empty($instance['settings']['file_resup']) && $instance['settings']['file_resup']) {
      // Most file_resup settings belong on the parent element.
      $element['#file_resup_max_files'] = 1;
      $element['#field_name'] = $field['field_name'];

      // Validators are read from both the parent and the child, so set in both places.
      $element['file']['#file_resup_upload_validators'] = $element['file']['#upload_validators'];
      $element['#file_resup_upload_validators'] = $element['file']['#upload_validators'];

      // Set our version of the file_resup #process callback.
      $element['#process'] = array('soundcloud_file_field_resup_widget_process');
      // We can use file_resup's own field_value callback.
      $element['file']['#file_value_callbacks'][] = 'file_resup_field_widget_value';

      // This is called from both the parent and the child. It's already set in the child, add it to the parent.
      $element['#upload_location'] = $instance['settings']['uri_scheme'] . '://' . $instance['settings']['file_directory'];
    }

    // Soundcloud URL.
    $url_present = isset($items[$delta]['url']) && !empty($items[$delta]['url']);
    $queued_present = isset($items[$delta]['queued']) && !empty($items[$delta]['queued']);

    if ($url_present || !$queued_present) {
      $element['url'] = array(
        '#title' => t('Soundcloud URL'),
        '#type' => 'textfield',
        '#default_value' => $url_present ? $items[$delta]['url'] : NULL,
        '#description' => t('Leave this blank to have your file sent to Soundcloud, and the URL generated automatically for you.'),
        '#weight' => 0,
      );
    }
    else {
      // Queued timestamp
      if ($queued_present) {
        $queued_date = format_date($items[$delta]['queued'], 'short');
        $element['queued'] = array(
          '#prefix' => '<div class="soundcloud-file-field-queued">',
          '#markup' => t('This file was queued for upload at !queued', array('!queued' => $queued_date)),
          '#suffix' => '</div>',
          '#weight' => 1,
          '#value' => $items[$delta]['queued'],
        );
      }
    }
  }

  return $element;
}

/**
 * Processor for the file resup widget.
 * @see file_resup_field_widget_process().
 */
function soundcloud_file_field_resup_widget_process($element, &$form_state, $form)  {
  $path = drupal_get_path('module', 'file_resup');
  $max_files = $element['#file_resup_max_files'];

  // Autostart is always false, because we don't have a setting for it.
  $autostart = FALSE;

  // Get the upload validators and build a new description.
  if (isset($element['#bundle'])) {
    $field = field_widget_field($element, $form_state);
    $instance = field_widget_instance($element, $form_state);
    $description = $field['cardinality'] == 1 ? field_filter_xss($instance['description']) : '';
  }
  else {
    $description = $element['#description'];
  }
  $upload_validators = $element['file']['#file_resup_upload_validators'];
  $description = theme('file_upload_help', array('description' => $description, 'upload_validators' => $upload_validators));
  // Add the resup element.
  // NB: some of these originally depended on file_managed_file_process running first... which won't happen with a child field.
  // In those cases I tried to derive the value the same way in here instead.
  $element['file']['resup'] = array(
    '#type' => 'hidden',
    '#value_callback' => 'file_resup_value',
    '#field_name' => $element['#field_name'],
    '#field_parents' => $element['#field_parents'],
    '#upload_location' => $element['file']['#upload_location'],
    '#file_resup_upload_validators' => $upload_validators,
    '#attributes' => array(
      'class' => array('file-resup'),
      'data-upload-name' => 'files[' . implode('_', $element['#parents']) . '_file]',
      'data-upload-button-name' => implode('_', $element['#parents']) . '_file_upload_button',
      'data-max-filesize' => $upload_validators['file_validate_size'][0],
      'data-description' => $description,
      'data-url' => url('file_resup/upload/' . implode('/', $element['#array_parents']) . '/' . $form['form_build_id']['#value']),
      'data-drop-message' => $max_files > -1 ? format_plural($max_files, 'Drop a file here or click <em>Browse</em> below.', 'Drop up to @count files here or click <em>Browse</em> below.') : t('Drop files here or click <em>Browse</em> below.'),
    ),
    '#prefix' => '<div class="file-resup-wrapper">',
    '#suffix' => '</div>',
    '#attached' => array(
      'css' => array($path . '/file_resup.css'),
      'js' => array(
        $path . '/js/resup.js',
        $path . '/file_resup.js',
        array(
          'type' => 'setting',
          'data' => array('file_resup' => array('chunk_size' => file_resup_chunksize())),
        ),
      ),
    ),
  );

  // Set the upload location on the parent element, because that's the only change needed to use file_resup's own uploading menu callback.
  $element['#upload_location'] = $element['file']['#upload_location'];

  // Add the extension list as a data attribute.
  if (isset($upload_validators['file_validate_extensions'][0])) {
    $extension_list = implode(',', array_filter(explode(' ', $upload_validators['file_validate_extensions'][0])));
    $element['file']['resup']['#attributes']['data-extensions'] = $extension_list;
  }

  // Add the maximum number of files as a data attribute.
  if ($max_files > -1) {
    $element['file']['resup']['#attributes']['data-max-files'] = $max_files;
  }

  // Add autostart as a data attribute.
  if ($autostart) {
    $element['file']['resup']['#attributes']['data-autostart'] = 'on';
  }

  $element['file']['upload_button']['#submit'][] = 'soundcloud_file_field_resup_widget_submit';
  $element['file']['#pre_render'][] = 'soundcloud_file_field_resup_widget_pre_render';

  return $element;
}

/**
 * #pre_render callback for the field widget element.
 */
function soundcloud_file_field_resup_widget_pre_render($element) {
  if (!empty($element['file']['#value']['fid'])) {
    $element['file']['resup']['#access'] = FALSE;
  }
  return $element;
}

/**
 * Our version of file_resup_widget_submit.
 * Identical except that it uses another method to get the field status.
 * @see file_resup_widget_submit().
 */
function soundcloud_file_field_resup_widget_submit($form, &$form_state) {
  $button = $form_state['triggering_element'];
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
  $field_name = $element['#field_name'];
  $langcode = $element['#language'];
  $parents = $element['#field_parents'];
  $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);

  if (!empty($field_state)) {
    $items = $field_state['items'];
  }
  elseif (!empty($form_state['values'][$field_name])) {
    $items[] = array('fid' => $form_state['values'][$field_name]);
    // If it's an extended field, look for subfield name variable and use that.
    if (isset($form[$field_name]['#extended']) && $form[$field_name]['#extended'] && !empty($element['#subfield_name'])) {
      $subfield_name = $element['#subfield_name'];
      if (!empty($form_state['values'][$field_name][$subfield_name])) {
        $items[] = array('fid' => $form_state['values'][$field_name][$subfield_name]);
      }

    }
  }

  // Remove possible duplicate items.
  $fids = array();
  foreach ($items as $delta => $item) {
    if (in_array($item['fid'], $fids)) {
      unset($items[$delta]);
    }
    else {
      $fids[] = $item['fid'];
    }
  }
  $items = array_values($items);

  // Append our items.
  if (!empty($element['resup']['#value'])) {
    $fids = array_diff(explode(',', $element['resup']['#value']), $fids);
    foreach ($fids as $fid) {
      $items[] = array('fid' => $fid);
    }
  }

  drupal_array_set_nested_value($form_state['values'], array_slice($button['#array_parents'], 0, -2), $items);
  $field_state['items'] = $items;
  field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);
}

/**
 * Validation for soundcloud_file fields with our widget.
 * If there's no URL entered in the form, add the file to the queue to be submitted
 * to sondcloud.
 *
 * Also reformats the $item to just have an FID, rather than a file array with an FID in it.
 * This lets us use file api's own storage handling.
 */
function _soundcloud_file_field_validate($element, &$form_state, $form) {
  // form_set_value expects to save an array as our field value. Get it started.
  $value_to_save = array(
    'fid' => $element['file']['#value']['fid'],
  );

  // If there's no URL.
  if (!isset($element['url']) || empty($element['url']['#value'])) {
    // If it hasn't been queued yet, queue it up!
    if (!isset($element['queued'])) {
      // Load the actual file from FID.
      $file = file_load($element['file']['#value']['fid']);
      if ($file) {
        // Add the file to the queue, and put the timestamp value in.
        $queue = DrupalQueue::get('soundcloud_file_field_uploads');
        // Values to insert into the queue.
        $data = array(
          'type' => $element['#entity_type'],
          'bundle' => $element['#bundle'],
          'field' => $element['#field_name'],
          'fid' => $file->fid,
        );
        // Insert them into the queue.
        if ($queue->createItem($data)) {
          $value_to_save['queued'] = REQUEST_TIME;
        }
      }
    }
    // If it HAS been queued, preserve the queued timestamp.
    else {
      $value_to_save['queued'] = $element['queued']['#value'];
    }
  }
  // If there IS a URL.
  else {
    // Explicitly set a null queued timestamp.
    $value_to_save['queued'] = NULL;
    $value_to_save['url'] = $element['url']['#value'];
  }
  // Save whatever field value we ended up with.
  form_set_value($element, $value_to_save, $form_state);
}

/**
 * Implements hook_field_formatter_info().
 */
function soundcloud_file_field_field_formatter_info() {
  return array(
    'soundcloud_file_field_default' => array(
      'label' => t('Flash player'),
      'field types' => array('soundcloud_file_field'),
    ),

    'soundcloud_file_field_html5' => array(
      'label' => t('HTML5 player'),
      'field types' => array('soundcloud_file_field'),
      'settings' => array(
        // Give a default value for when the form is first loaded.
        'player_type' => 'classic',
      ),
    ),

    'soundcloud_file_field_link' => array(
      'label' => t('Link'),
      'field types' => array('soundcloud_file_field'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function soundcloud_file_field_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  // This gets the view_mode where our settings are stored.
  $display = $instance['display'][$view_mode];
  // This gets the actual settings.
  $settings = $display['settings'];
  // Initialize the element variable.
  $element = array();
  // Add select box, for HTML5 only.
  if (strpos($display['type'], '_html5') !== FALSE) {
    $element['player_type'] = array(
      '#type' => 'select',
      '#title' => t('HTML5 player type'),
      '#description' => t('Select which HTML5 player to use.'),
      '#default_value' => $settings['player_type'],
      '#options' => array(
        'classic' => 'Classic (old)',
        'visual' => 'Visual Player (new)',
      ),
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function soundcloud_file_field_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = '';

  if (strpos($display['type'], '_html5') !== FALSE) {
    $summary = t('Use the ') . $settings['player_type'] . t(' HTML5 player.');
  }

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function soundcloud_file_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  // if player custom settings array is not available, use default settings
  if (!empty($instance['settings']['soundcloudplayer'])) {
    $settings = $instance['settings']['soundcloudplayer'];
  }
  else {
    $settings = $instance['settings'];
    watchdog('soundcloud_file_field', 'Instance settings array is missing. Falling back to default player settings.', NULL, WATCHDOG_WARNING);
  }

  // Get the "common" settings.
  $autoplay = ($settings['autoplay']) ? 'true' : 'false';
  $showplaycount = ($settings['showplaycount']) ? 'true' : 'false';
  $showartwork = ($settings['showartwork']) ? 'true' : 'false';
  $color = $settings['color'];

  switch ($display['type']) {
    // Flash player case.
    case 'soundcloud_file_field_default':
      // Get the 'showcomment' setting for the flash player.
      $showcomments = ($settings['flash_player']['showcomments']) ? 'true' : 'false';

      foreach ($items as $delta => $item) {
        if (!empty($item['url'])) {
          $output = '';
          $encoded_url = urlencode($item['url']);
          $parsed_url = parse_url($item['url']);

          $splitted_url = explode("/", $parsed_url['path']);

          // If this is a track..
          if ($splitted_url[2] != 'sets') {
            $height = $settings['flash_player']['height'];
            $params = 'url=' . $encoded_url .
              '&amp;show_comments=' . $showcomments .
              '&amp;auto_play=' . $autoplay .
              '&amp;color=' . $color;
          }
          // Otherwise it's a set.
          else {
            $height = $settings['flash_player']['height_sets'];
            $params = 'url=' . $encoded_url .
              '&amp;show_comments=' . $showcomments .
              '&amp;auto_play=' . $autoplay .
              '&amp;show_playcount=' . $showplaycount .
              '&amp;show_artwork=' . $showartwork .
              '&amp;color=' . $color;
          }

          $output = '<object height="' . $height . '" width="' . $settings['width'] . '%"><param name="movie" value="https://player.' . $parsed_url['host'] . '/player.swf?' . $params . '"></param>';
          $output .= '<param name="allowscriptaccess" value="always"></param><embed allowscriptaccess="always" height="' . $height . '" src="https://player.' . $parsed_url['host'] . '/player.swf?' . $params . '" type="application/x-shockwave-flash" width="' . $settings['width'] . '%"></embed></object>';

          $element[$delta] = array('#markup' => $output);
        }
      }
      break;
    // HTML5 (classic+visual) player case.
    case 'soundcloud_file_field_html5':
      // Add the SoundCloud Widget API, for pausing in playlists.
      drupal_add_js('https://w.soundcloud.com/player/api.js', 'external');

      $oembed_endpoint = 'http://soundcloud.com/oembed';

      // Get HTML5 player-specific settings.
      $html5_player_height = (empty($settings['html5_player']['html5_player_height']) ? SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT : $settings['html5_player']['html5_player_height']);
      $html5_player_height_sets = (empty($settings['html5_player']['html5_player_height_sets']) ? SOUNDCLOUD_FILE_FIELD_DEFAULT_HTML5_PLAYER_HEIGHT_SETS : $settings['html5_player']['html5_player_height_sets']);
      $visual_player = ($display['settings']['player_type'] == 'visual' ? 'true' : 'false');

      foreach ($items as $delta => $item) {
        if (!empty($item['url'])) {
          $output = '';

          // Set the proper height for this item.
          // - classic player: track default is 166px, set default is 450px.
          // - visual player: player height it's the same for tracks and sets.
          if ($visual_player == 'true') {
            $iframe_height = $settings['visual_player']['visual_player_height'];
          }
          else {
            $parsed_url = parse_url($item['url']);
            $splitted_url = explode("/", $parsed_url['path']);
            // Track or set?
            $iframe_height = ($splitted_url[2] != 'sets') ? $html5_player_height : $html5_player_height_sets;
          }

          $oembed = soundcloud_file_field_soundcloud_metadata($item['url']);
          if ($oembed !== FALSE) {
            // Replace player default settings with our settings,
            // set player width and height first.
            $final_iframe = preg_replace('/(width=)"([^"]+)"/', 'width="' . $settings['width'] . '%"', $oembed->html);
            $final_iframe = preg_replace('/(height=)"([^"]+)"/', 'height="' . $iframe_height . '"', $oembed->html);
            // Set autoplay.
            if (preg_match('/auto_play=(true|false)/', $final_iframe)) {
              $final_iframe = preg_replace('/auto_play=(true|false)/', 'auto_play=' . $autoplay, $final_iframe);
            }
            else {
              $final_iframe = preg_replace('/">/', '&auto_play=' . $autoplay . '">', $final_iframe);
            }
            // Show playcount?
            if (preg_match('/show_playcount=(true|false)/', $final_iframe)) {
              $final_iframe = preg_replace('/show_playcount=(true|false)/', 'show_playcount=' . $showplaycount, $final_iframe);
            }
            else {
              $final_iframe = preg_replace('/">/', '&show_playcount=' . $showplaycount . '">', $final_iframe);
            }
            // Show artwork?
            if (preg_match('/show_artwork=(true|false)/', $final_iframe)) {
              $final_iframe = preg_replace('/show_artwork=(true|false)/', 'show_artwork=' . $showartwork, $final_iframe);
            }
            else {
              $final_iframe = preg_replace('/">/', '&show_artwork=' . $showartwork . '">', $final_iframe);
            }
            // Set player color.
            if (preg_match('/color=([a-zA-Z0-9]{6})/', $final_iframe)) {
              $final_iframe = preg_replace('/color=([a-zA-Z0-9]{6})/', 'color=' . $color, $final_iframe);
            }
            else {
              $final_iframe = preg_replace('/">/', '&color=' . $color . '">', $final_iframe);
            }
            // Set HTML5 player type based on formatter: classic/visual player.
            if (preg_match('/visual=(true|false)/', $final_iframe)) {
              $final_iframe = preg_replace('/visual=(true|false)/', 'visual=' . $visual_player, $final_iframe);
            }
            else {
              $final_iframe = preg_replace('/">/', '&visual=' . $visual_player . '">', $final_iframe);
            }
            // Final output. Use '$oembed->html' for original embed code.
            $output = html_entity_decode($final_iframe);
          }
          else {
            $output = t('The SoundCloud content at !url is not available, or it is set to private.', array('!url' => l($item['url'], $item['url'])));
          }

          $element[$delta] = array('#markup' => $output);
        }
      }
      break;
    // Link formatter.
    case 'soundcloud_file_field_link':
      foreach ($items as $delta => $item) {
        if (!empty($item['url'])) {
          $text = $item['url'];

          // if we have valid soundcloud info, use that instead
          $meta = soundcloud_file_field_soundcloud_metadata($item['url']);
          if ($meta !== FALSE) {
            $text = $meta->title;
          }

          $element[$delta] = array(
            '#theme' => 'soundcloud_thumbnail',
            '#link' => $item['url'],
            '#title' => $text,
          );
        }
      }
      break;
  }

  return $element;
}

/**
 * Implement hook_field_error().
 */
function soundcloud_file_field_field_widget_error($element, $error) {
  switch ($error['error']) {
    case 'soundcloud_file_field_invalid_url':
      form_error($element, $error['message']);
      break;
  }
}