<?php

/**
 * Class SoundcloudClient
 */

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Stream;

class SoundcloudClient extends \Drupal\oauth2_client_service\OAuth2ClientService\Connection {
  /**
   * The guzzle client object.
   */
  protected $guzzle;

  public function getTokenServerSide() {
    if (!isset($_GET['code'])) {
      drupal_goto($this->getAuthenticationUrl());
    }
    else {
      // SoundCloud API requires client ID and secret
      return $this->getToken(array(
        'client_id' => $this->params['client_id'],
        'client_secret' => $this->params['client_secret'],
        'grant_type' => 'authorization_code',
        'code' => $_GET['code'],
        'redirect_uri' => $this->params['redirect_uri'],
      ));
    }
  }

  /**
   * Normal request method- just adds the oauth token as a URL parameter. Pretty
   * sure this will go unused though, because drupal_http_request is an http/1.0
   * function, and 1.0 sucks at handling large files.
   * @see sendFile
   *
   * @inheritdoc
   */
  public function request($method, $endpoint, array $parameters = array()) {
    $endpoint .= '?' . drupal_http_build_query(array('oauth_token' => $this->getAccessToken()));
    return parent::request($method, $endpoint, $parameters);
  }

  /**
   * Special request method based on Guzzle
   *
   * @param array $metadata
   *   An array of metadata to send for the file. Keys:
   *    - title (required)
   *    - description
   *    - tag_list (double quoted, space separated list of tags)
   * @param \StdClass $file (the Drupal file object to send)
   * @return mixed|\Psr\Http\Message\ResponseInterface
   * @throws \Exception
   */
  public function sendFile(array $metadata, StdClass $file) {
    // Validate input
    if (!isset($metadata['title']) || empty($metadata['title'])) {
      throw new Exception('Track title is required');
    }

    // Endpoint is always the same.
    $endpoint = 'https://api.soundcloud.com/tracks';

    $client = $this->getGuzzle();

    // Load up the multipart data for Guzzle.
    $data = array();

    // Now add the track metadata.
    foreach ($metadata as $key => $value) {
      $data['multipart'][] = array(
        'name' => 'track[' . $key . ']',
        'contents' => $value,
      );
    }

    // Get the file resource. Support local and remote streams.
    $file_uri = drupal_realpath($file->uri) ? drupal_realpath($file->uri) : file_create_url($file->uri);

    // Try to open the file for reading.
    $file_handler = fopen($file_uri, 'r');

    // If the file doesn't open, throw an exception.
    if (!$file_handler) {
      throw new Exception('Could not open the file at ' . $file_handler);
    }

    // Create a file stream, so we can set the filesize explicitly. This allows support for remote streams.
    $file_stream = new Stream($file_handler, array('size' => $file->filesize));

    // Add the file to our data array.
    $data['multipart'][] = array(
      'name' => 'track[asset_data]',
      'contents' => $file_stream,
    );

    return $client->request('POST', $endpoint, $data);

  }

  /**
   * Custom token storage in a variable
   * @inheritdoc
   */
  protected function storeToken($token) {
    variable_set('soundcloud_file_field_token', $token);
  }

  /**
   * Custom token storage retrieval from variable
   * @inheritdoc
   */
  protected function getStoredToken() {
    return variable_get('soundcloud_file_field_token', array());
  }

  /**
   * Utility function to create a Guzzle client with our credentials preloaded.
   * @param string $path The soundcloud path to hit, no leading slash! Default: "/tracks"
   * @return GuzzleHttp\Client
   */
  public function getGuzzle() {

    if (!$this->guzzle) {
      // Bearer auth header.
      $bearer_token = 'Bearer ' . $this->getAccessToken();

      // Create the Guzzle client
      global $base_url;
      $this->guzzle = new Client(
        array(
          'base_uri' => $base_url,
          'headers' => array(
            'Authorization' => $bearer_token,
          ),
          'query' => array(
            'oauth_token' => $this->getAccessToken(),
          ),
        )
      );
    }

    return $this->guzzle;
  }

  /**
   * @param $date (yyyy-mm-dd hh:mm:ss) return tracks created at this date or later
   */
  function listTracksCreatedSince($date) {

    $endpoint = 'https://api.soundcloud.com/users/me/tracks' ;

    $options['query'] = array(
      'oauth_token' => $this->getAccessToken(),
      'order' => 'created_at',
      'limit' => 200,
      'created_at[from]' => $date,
    );

    try {
      $response = $this->getGuzzle()->request("GET", $endpoint, $options);
    }
    catch (Exception $e) {
      throw $e;
    }
    $track_list = array();
    if ($response->getStatusCode() == '200') {
      $response_data = json_decode($response->getBody());
      // Build the list of track IDs.
      foreach ($response_data as $track) {
        $track_list[] = $track->id;
      }
    }
    return $track_list;
  }

  /**
   * Delete tracks.
   * @param array $track_list  A list of track ID numbers.
   * @throws \Exception
   * @return TRUE on success
   */
  public function deleteTracks($track_list = array()) {
    $count = 0;
    foreach ($track_list as $track) {
      if ($this->deleteTrack($track)) {
        $count++;
        continue;
      }
      else {
        return FALSE;
      }
    }
    watchdog('soundcloud_file_field', 'Deleted %count tracks from Soundcloud', array('%count' => $count));

    return TRUE;
  }

  /**
   * @param $track_id
   * @return bool TRUE on success
   * @throws \Exception
   */
  public function deleteTrack($track_id) {

    $endpoint = 'https://api.soundcloud.com/tracks/' . $track_id;

    $options['query'] = array(
      'oauth_token' => $this->getAccessToken(),
    );

    try {
      $response = $this->getGuzzle()->request("DELETE", $endpoint, $options);
    }
    catch (Exception $e) {
      throw $e;
    }

    return TRUE;
  }
}